$("#p-actual").change(function(){
  $(this).find("option:selected").each(function(){
    // Puesto actual uno
    $(".pd-selector").hide();
    if($(this).attr("value")=="actual-uno"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #uno");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-dos").show();
      $(".pd-tres").show();
      $(".pd-siete").show();
    }
    // Puesto actual dos
    else if($(this).attr("value")=="actual-dos"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #dos");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-uno").show();
      $(".pd-tres").show();
      $(".pd-siete").show();
    }
    // Puesto actual tres
    else if($(this).attr("value")=="actual-tres"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #tres");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-uno").show();
      $(".pd-dos").show();
      $(".pd-siete").show();
    }
    // Puesto actual cuatro
    else if($(this).attr("value")=="actual-cuatro"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #cuatro");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-cinco").show();
    }
    // Puesto actual cinco
    else if($(this).attr("value")=="actual-cinco"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #cinco");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-ocho").show();
      $(".pd-nueve").show();
    }
    // Puesto actual seis
    else if($(this).attr("value")=="actual-seis"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #seis");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-ocho").show();
      $(".pd-nueve").show();
    }
    // Puesto actual siete
    else if($(this).attr("value")=="actual-siete"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #siete");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-cinco").show();
      $(".pd-ocho").show();
      $(".pd-nueve").show();
    }
    // Puesto actual ocho
    else if($(this).attr("value")=="actual-ocho"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #ocho");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-nueve").show();
      $(".pd-diez").show();
      $(".pd-once").show();
      $(".pd-doce").show();
    }
    // Puesto actual nueve
    else if($(this).attr("value")=="actual-nueve"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #nueve");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-ocho").show();
      $(".pd-diez").show();
      $(".pd-once").show();
    }
    // Puesto actual diez
    else if($(this).attr("value")=="actual-diez"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #diez");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-once").show();
      $(".pd-catorce").show();
    }
    // Puesto actual once
    else if($(this).attr("value")=="actual-once"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #once");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-diez").show();
      $(".pd-catorce").show();
    }
    // Puesto actual doce
    else if($(this).attr("value")=="actual-doce"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #doce");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-catorce").show();
      $(".pd-quince").show();
    }
    // Puesto actual trece
    else if($(this).attr("value")=="actual-trece"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #trece");

      // Mostrar Horizontales y Verticales habilitados;
      $(".pd-quince").show();
    }
    // Puesto actual catorce
    else if($(this).attr("value")=="actual-catorce"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #catorce");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-quince").show();
    }
    // Puesto actual quince
    else if($(this).attr("value")=="actual-quince"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #quince");

      // Mostrar Horizontales y Verticales habilitados
      $(".pd-diecisiete").show();
      $(".pd-dieciocho").show();
    }
    // Puesto actual dieciseis
    else if($(this).attr("value")=="actual-dieciseis"){
      $("#tabla-puesto-actual").load("../plan-carrera-continental/tablas-competencias.html #dieciseis");
    }
    else{
      $(".box-actual").hide();
    }
  });
}).change();

$("#p-deseado").change(function(){
  $(this).find("option:selected").each(function(){
    // Puesto deseado uno
    if($(this).attr("value")=="deseado-uno"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #uno");
    }
    // Puesto deseado dos
    else if($(this).attr("value")=="deseado-dos"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #dos");
    }
    // Puesto deseado tres
    else if($(this).attr("value")=="deseado-tres"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #tres");
    }
    // Puesto deseado cuatro
    else if($(this).attr("value")=="deseado-cuatro"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #cuatro");
    }
    // Puesto deseado cinco
    else if($(this).attr("value")=="deseado-cinco"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #cinco");
    }
    // Puesto deseado seis
    else if($(this).attr("value")=="deseado-seis"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #seis");
    }
    // Puesto deseado siete
    else if($(this).attr("value")=="deseado-siete"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #siete");
    }
    // Puesto deseado ocho
    else if($(this).attr("value")=="deseado-ocho"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #ocho");
    }
    // Puesto deseado nueve
    else if($(this).attr("value")=="deseado-nueve"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #nueve");
    }
    // Puesto deseado diez
    else if($(this).attr("value")=="deseado-diez"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #diez");
    }
    // Puesto deseado once
    else if($(this).attr("value")=="deseado-once"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #once");
    }
    // Puesto deseado doce
    else if($(this).attr("value")=="deseado-doce"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #doce");
    }
    // Puesto deseado trece
    else if($(this).attr("value")=="deseado-trece"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #trece");
    }
    // Puesto deseado catorce
    else if($(this).attr("value")=="deseado-catorce"){
     $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #catorce");
    }
    // Puesto deseado quince
    else if($(this).attr("value")=="deseado-quince"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #quince");
    }
    // Puesto deseado dieciseis
    else if($(this).attr("value")=="deseado-dieciseis"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #dieciseis");
    }
    // Puesto deseado diecisiete
    else if($(this).attr("value")=="deseado-diecisiete"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #diecisiete");
    }
    // Puesto deseado dieciocho
    else if($(this).attr("value")=="deseado-dieciocho"){
      $("#tabla-puesto-deseado").load("../plan-carrera-continental/tablas-competencias.html #dieciocho");
    }
    else{
      $(".box-deseado").hide();
    }
  });
}).change();
